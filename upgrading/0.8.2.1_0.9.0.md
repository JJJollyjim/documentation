# Upgrading from 0.8.2.1 to 0.9.0

First, read CHANGELOG.md to get familiar with the new release.

You **must** read the entirety of this document before starting the upgrade.

## Check replication status

During the upgrade CockroachDB will be running with one less node than usual.
Check now to make sure you have enough nodes in your pool to carry out the
upgrade and maintain availability.

## Pick a lead node

Pick one of your Hot Potato nodes to 'lead' the upgrade. This node will carry
out database migrations while the other nodes respond to notifications on the
old version.

## Copy the database

On your lead node create a copy of your database to do the upgrade on. See the
[cockroach docs](https://www.cockroachlabs.com/docs/stable/backup-and-restore.html)
for how to do this.

## Upgrade host and/or python version

If Hot Potato was running on Xenial, now is an ideal time to upgrade to Bionic.
Hot Potato 0.9.0 moves the officially supported Python version to 3.6.
Alternatively you can use unofficial builds or backports if an upgrade is not
feasible.

## Upgrade the lead node

1. Stop Hot Potato on your lead node.
2. Deploy the new Hot Potato code.
3. Update dependencies with Pipenv.
    ```bash
    $ pipenv sync
    ```
4. Update the configuration file to point at the correct (to upgrade) database
5. Add method and provider settings to the config:

    ```ini
    [MODICA]
    ENABLED = true
    ...

    [TWILIO]
    ENABLED = true
    ...

    [PUSHOVER]
    ENABLED = true
    ...

    [SMS]
    ENABLED = true

    [PAGER]
    ENABLED = true

    [APP]
    ENABLED = true
    ```
6. Update the database URL in the config file:

    Before: 
    ```ini
    [SQLALCHEMY]
    DATABASE_URI=cockroachdb://user:pass@server:port/database
    ```
    After:
    ```ini
    [COCKROACH]
    SERVER=server
    PORT=port
    DATABASE=database
    USERNAME=user
    PASSWORD=pass
    ```
7. Install npm
8. In `theme/` run `npm install --production` to install front-end dependencies
9.  In `theme/` run `NODE_ENV=production npm run build` to build front-end dependencies
10. Run migrations:
    ```bash
    $ FLASK_APP=app/app.py flask db upgrade
    ```

    This may take a long time depending on the size of your database.
11. Check the migration succeeded, and the database is in a good state
12. Stop Hot Potato (**NOT CockroachDB**) on all other nodes and start Hot
    Potato on the lead node. Consider using Anycast to eliminate service
    unavailability during this period.

## Upgrade all other nodes

1.  Stop Hot Potato
2.  Check out the new code
3.  Update dependencies with Pipenv
4.  Update the configuration file
5.  Install npm and build the front-end dependencies
6.  Start Hot Potato

## Catch up the database

If necessary, update the new database with changes made during the upgrade.
Currently this must be done manually.